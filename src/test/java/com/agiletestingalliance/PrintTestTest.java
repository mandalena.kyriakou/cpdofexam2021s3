package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PrintTestTest {

    @Test
    public void testGstr() {
        PrintTest printTest = new PrintTest("test");
        assertEquals("test", printTest.gstr());
    }
}
