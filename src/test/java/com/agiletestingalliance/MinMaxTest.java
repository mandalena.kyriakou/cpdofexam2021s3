package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MinMaxTest {

    @Test
    public void testFindMax() {
        MinMax minMax = new MinMax();
        assertEquals(10, minMax.findMax(5, 10));
        assertEquals(20, minMax.findMax(20, 10));
        assertEquals(30, minMax.findMax(30, 30));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("test", minMax.bar("test"));
        assertNull(minMax.bar(""));
        assertNull(minMax.bar(null));
    }
}
